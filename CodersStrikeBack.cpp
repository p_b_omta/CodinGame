#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <algorithm>
#include <limits>
#include <utility>
#include <cmath>
#include <chrono>

using namespace std;

constexpr int checkpoint_radius = 600; // units
constexpr int shield_radius = 400; // units
constexpr int rotation_rate = 18; // degrees / turn
constexpr int max_accel = 200; // units / turn / turn
constexpr float friction_frac = 0.15; // fraction of speed lost due to friction

namespace math
{
  template <typename T>
  constexpr T sgn(T x)
  {
    return x < 0 ? -1 : 1;
  }
}
using namespace math;

namespace geom
{
  // Point
  // -----
  // Can also be seen as a vector
  struct Point
  {
    int x, y;
  };

  bool operator== (Point a, Point b) { return a.x == b.x && a.y == b.y; }
  bool operator!= (Point a, Point b) { return a.x != b.x || a.y != b.y; }
  Point operator- (Point a, Point b) { return Point{a.x - b.x, a.y - b.y}; }
  Point operator+ (Point a, Point b) { return Point{a.x + b.x, a.y + b.y}; }
  Point operator- (Point a) { return Point{-a.x, -a.y}; }

  template <typename T>
  Point make_point(T x, T y)
  {
    return Point{static_cast<int>(x), static_cast<int>(y)};
  }

  Point invalid_point()
  {
    return Point{numeric_limits<int>::max(), numeric_limits<int>::max()};
  }

  float dist(Point a, Point b)
  {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    return sqrt(dx*dx + dy*dy);
  }

  float magnitude(Point a)
  {
    return sqrt(float(a.x)*a.x + float(a.y)*a.y);
  }

  // Magnitude of vector = 100, instead of 1.
  // To prevent rounding errors.
  Point semi_normalize(Point a)
  {
    if (a.x == 0 && a.y == 0)
      return a;
    float x = a.x / magnitude(a) * 100.0f;
    float y = a.y / magnitude(a) * 100.0f;
    return Point{int(x), int(y)};
  }

  // Segment
  // -------
  struct Segment
  {
    Point a, b;
  };

  int side(Point p, Segment s)
  {
    return sgn((s.b.x - s.a.x) * (p.y - s.a.y) - (s.b.y - s.a.y) * (p.x - s.a.x));
  }

  // Circle
  // ------
  struct Circle
  {
    Point center;
    float radius;
  };

  // Calculate intersection points of line through segment with circle.
  pair<Point, Point> intersection(Segment s, Circle c)
  {
    // Translate segment into circle center coordinate space (circle center = 0,0)
    s.a = s.a - c.center;
    s.b = s.b - c.center;
    float dx = s.b.x - s.a.x;
    float dy = s.b.y - s.a.y;
    float dr = sqrt(dx*dx + dy*dy);
    float D = s.a.x * s.b.y - s.b.x * s.a.y;
    float discr = c.radius*c.radius * dr*dr - D*D;

    if (discr <= 0.0)
      return make_pair(invalid_point(), invalid_point());

    float x1 = (D*dy + sgn(dy)*dx*sqrt(discr)) / (dr*dr);
    float y1 = (-D*dx + abs(dy)*sqrt(discr)) / (dr*dr);
    float x2 = (D*dy - sgn(dy)*dx*sqrt(discr)) / (dr*dr);
    float y2 = (-D*dx - abs(dy)*sqrt(discr)) / (dr*dr);

    return make_pair(make_point(x1, y1), make_point(x2, y2));
  }

  constexpr float PI = atan(1)*4.0;

  // Angle
  // -----
  constexpr float deg2rad(float a) { return a / 180 * PI; }
  constexpr float rad2deg(float a) { return a * 180 / PI; }

  struct Angle
  {
    float a; // (-180..180] degrees
    // pos x-axis = 0, pos down.

    Angle() : a(0) { }
    explicit Angle(float angle) : a(angle) { corr_angle(); }
    explicit Angle(Point v) { a = rad2deg(atan2(v.y, v.x)); }

    Angle operator+ (Angle rhs) const { return Angle(a + rhs.a); }
    Angle operator- (Angle rhs) const { return Angle(a - rhs.a); }

    private:
      void corr_angle() {
        while (a > 180)
          a -= 360;
        while (a <= -180)
          a += 360;
      }
  };

  Angle bearing(Point a, Point b)
  {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    return Angle(rad2deg(atan2(dy, dx)));
  }
}
using namespace geom;

// POD class
// ---------
struct Pod
{
  Point p; // Location
  Point v; // Speed vector
  Angle h; // Heading
  unsigned next_cp = 0; // next checkpoint
  int progress = -1;

  void read_from_stdin() {
    unsigned last_cp = next_cp;

    int hding;
    cin >> p.x >> p.y >> v.x >> v.y >> hding >> next_cp;
    cin.ignore();

    h = Angle(hding);
    if (next_cp != last_cp)
      ++progress;
  }

  void debug_print() const {
    cerr<<"spd:"<<int(speed())
        <<" hdg:"<<int(h.a)
        <<" dir:"<<int(direction().a)<<endl;
  }

  // Location next turn
  Point p1() const {
    return p + v;
  }

  float speed() const {
    return magnitude(v);
  }

  Angle direction() const {
    if (v.x == 0 && v.y == 0)
      return h;
    return Angle(v);
  }

  bool will_collide_with(Pod const & b) const {
    if (dist(p1(), b.p1()) <= 2*shield_radius)
      return true;
    Point thrust_vec{int(max_accel * cos(deg2rad(h.a))), int(max_accel * sin(deg2rad(h.a)))};
    if (dist(p1() + thrust_vec, b.p1()) <= 2*shield_radius)
      return true;
    return false;
  }
};

// Nr of turns to reach checkpoint without thrust.
int eta(Pod const & pod, vector<Point> const & cps)
{
  if (pod.v.x == 0 && pod.v.y == 0)
    return numeric_limits<int>::max();

  auto tgts = intersection(Segment{pod.p, pod.p1()}, Circle{cps[pod.next_cp], checkpoint_radius});
  if (tgts.first == invalid_point())
    return numeric_limits<int>::max();

  tgts.first = tgts.first + cps[pod.next_cp];
  tgts.second = tgts.second + cps[pod.next_cp];

  float d1 = dist(pod.p, tgts.first);
  float d2 = dist(pod.p, tgts.second);
  float d = min(d1, d2); // The distance to travel
  float speed = pod.speed();
  int turns = 0;
  while (d > 0.0f) {
    d -= speed;
    speed *= (1.0 - friction_frac);
    if (speed < max_accel * 2)
      return numeric_limits<int>::max();
    ++turns;
  }
  return turns;
}

// Nr of turns for pod to reach target t, with the given thrust.
// Without overshooting the target.
// Returns max unsigned if it can't be reached.
unsigned predict3(Pod const & pod, Point t, int thrust)
{
  unsigned turn = 0;

  Point p = pod.p; // point
  Point v = pod.v; // speed vector
  Angle h = pod.h; // heading

  float total_rotation = 0;

  while(dist(p, t) > checkpoint_radius && total_rotation < 360) {
    // Rotate
    auto tgts = intersection(Segment{p, t}, Circle{p + v, max_accel} );
    if (tgts.first == invalid_point()) {
      Point tgt = semi_normalize(-v) + semi_normalize(t - p);

      // Rotate
      Angle delta = Angle(tgt) - h;
      if (delta.a > rotation_rate) delta = Angle(rotation_rate);
      if (delta.a < -rotation_rate) delta = Angle(-rotation_rate);
      h = h + delta;
      total_rotation += abs(delta.a);
      // Apply thrust
      v.x = v.x + thrust * cos(deg2rad(h.a));
      v.y = v.y + thrust * sin(deg2rad(h.a));
      p = p + v;
      // Friction
      v.x = v.x * (1.0 - friction_frac);
      v.y = v.y * (1.0 - friction_frac);
    }
    else {
      Point tt = t - (p + v);
      Point v_corr = dist(tgts.first, tt) < dist(tgts.second, tt) ? tgts.first : tgts.second;
      // Rotate
      Angle delta = Angle(v_corr);
      if (delta.a > rotation_rate) delta = Angle(rotation_rate);
      if (delta.a < -rotation_rate) delta = Angle(-rotation_rate);
      h = h + delta;
      total_rotation += abs(delta.a);
      // Apply thrust
      v.x = v.x + max_accel * cos(deg2rad(h.a));
      v.y = v.y + max_accel * sin(deg2rad(h.a));
      p = p + v;
      // Friction
      v.x = v.x * (1.0 - friction_frac);
      v.y = v.y * (1.0 - friction_frac);
    }

    ++turn;
  }
  return turn;
}

int next_checkpoint(int cp, int nr_cps)
{
  int next = cp + 1;
  if (next == nr_cps)
    next = 0;
  return next;
}

// Speeder AI
// ----------
void speeder_AI(
    Pod const & speeder,
    array<Pod, 2> const & enemies,
    vector<Point> const & cps)
{
  cerr<<"Speeder "; speeder.debug_print();
  Point speeder_tgt = invalid_point();
  int speeder_thrust = max_accel;
  string message;

  int next_next_cp = next_checkpoint(speeder.next_cp, cps.size());
  Angle next_bearing = bearing(cps[speeder.next_cp], cps[next_next_cp]);
  int turn_time = abs(Angle(next_bearing - speeder.h).a) / rotation_rate;

//  bool enemy0_close = dist(enemies[0].p1(), speeder.p) < 5 * shield_radius;
//  bool enemy1_close = dist(enemies[1].p1(), speeder.p) < 5 * shield_radius;
//  bool enemy0_incoming = abs((enemies[0].h - bearing(enemies[0].p, speeder.p)).a) < rotation_rate / 2;
//  bool enemy1_incoming = abs((enemies[1].h - bearing(enemies[1].p, speeder.p)).a) < rotation_rate / 2;
//    if (enemy0_close && enemy0_incoming || enemy1_close && enemy1_incoming)  {
//    // Under attack
//    speeder_tgt = cps[speeder.next_cp]; // TODO: or 90-degree angle to attacker?
//    speeder_thrust = max_accel;
//    cerr<<"Speeder doing evasive maneuvres\n";
//    message = "Can't touch this!";
//  }
//  else
  if (eta(speeder, cps) <= turn_time) {
    // Nearing target with high speed, line-up speeder for next target.
    speeder_tgt = speeder.p +
                  semi_normalize(-speeder.v) +
                  semi_normalize(cps[next_next_cp] - speeder.p);
    speeder_thrust = 0;
    cerr<<"Speeder: skidding.\n";
  }
  else if (abs((bearing(speeder.p, cps[speeder.next_cp]) - speeder.direction()).a) < 45) {
    auto tgts = intersection(Segment{speeder.p, cps[speeder.next_cp]},
                             Circle{speeder.p1(), max_accel} );

    if (tgts.first != invalid_point()) {
      // Aggressively correct course to get on a straight line to the target ASAP.
      Point cp = cps[speeder.next_cp] - speeder.p1();
      Point tgt = dist(tgts.first, cp) < dist(tgts.second, cp) ? tgts.first : tgts.second;

      speeder_tgt = Point{ speeder.p.x + tgt.x, speeder.p.y + tgt.y };
      speeder_thrust = max_accel;
      cerr<<"Speeder: fancy course correction.\n";
    }
  }

  if (speeder_tgt == invalid_point()) {
    // Rotate the pod, search for ideal thrust.
    // Need to compensate direction vector
    speeder_tgt = speeder.p +
                  semi_normalize(-speeder.v) +
                  semi_normalize(cps[speeder.next_cp] - speeder.p);

    unsigned best_t = numeric_limits<unsigned>::max();
    int best_thrust;
    for (int thrust = max_accel; thrust > 0; thrust -= 1) {
      unsigned t = predict3(speeder, cps[speeder.next_cp], thrust);
      if (t < best_t) {
        best_t = t;
        best_thrust = thrust;
      }
      // FIXME: opmization opportunity here (early quit)
    }
    if (best_t == numeric_limits<unsigned>::max())
      speeder_thrust = 0;
    else
      speeder_thrust = best_thrust;

    cerr << "Speeder: hard turn (eta: " << best_t << " turns)\n";
  }

  // Activate shield if we're going fast and a collision is iminent.
  if (speeder.speed() > 2 * max_accel) {
    if (enemies[0].will_collide_with(speeder) ||
        enemies[1].will_collide_with(speeder))
      speeder_thrust = -1;
  }

  cout << speeder_tgt.x << " " << speeder_tgt.y << " ";
  if (speeder_thrust == -1)
    cout << "SHIELD";
  else
    cout << speeder_thrust;
  if (message.empty())
    cout << endl;
  else
    cout << " " << message << endl;
}

// Bludger AI
// ----------
void bludger_AI(
    Pod const & bludger,
    Pod const & speeder,
    array<Pod, 2> const & enemies,
    vector<Point> const &)
{
  cerr<<"Bludger "; bludger.debug_print();
  Point bludger_tgt;
  int bludger_thrust = max_accel;
  if (speeder.progress > enemies[0].progress + 1 &&
      speeder.progress > enemies[1].progress + 1) {
    // Are we ahead? Then hassle the probable attacker (least progress).
    if (enemies[0].progress < enemies[1].progress)
      bludger_tgt = enemies[0].p1();
    else
      bludger_tgt = enemies[1].p1();
    cerr << "Bludger hassling probable attacker\n";
  }
  else {
    if (enemies[0].progress == enemies[1].progress) {
      // Occurs mostly at start. Prevents bludger from hitting speeder.
      if (dist(enemies[0].p, bludger.p) < dist(enemies[1].p, bludger.p))
        bludger_tgt = enemies[0].p1();
      else
        bludger_tgt = enemies[1].p1();
      cerr << "Bludger attacking closest\n";
    }
    else if (enemies[0].progress > enemies[1].progress) {
      bludger_tgt = enemies[0].p1();
      cerr << "Bludger attacking enemy 1\n";
    }
    else {
      bludger_tgt = enemies[1].p1();
      cerr << "Bludger attacking enemy 2\n";
    }
  }

  if (dist(bludger.p, bludger_tgt) < 5 * shield_radius) {
    // We're close to the enemy, keep pounding him.
    Angle wanted_bearing = bearing(bludger.p, bludger_tgt);
    if (abs((wanted_bearing - bludger.h).a) < rotation_rate / 4)
      bludger_thrust = max_accel;
    else
      bludger_thrust = 50;
  }
  else {
    // Regular steering.
    auto tgts = intersection(Segment{bludger.p, bludger_tgt},
                             Circle{bludger.p1(), max_accel} );

    if (tgts.first == invalid_point()) {
      Angle wanted_bearing = bearing(bludger.p, bludger_tgt);
      int delta = abs((wanted_bearing - bludger.h).a);
      if (delta == 0)
        bludger_thrust = max_accel;
      else
        bludger_thrust = max_accel / delta;
    }
    else {
      // Correct course to get on a straight line to the target.
      Point tp = bludger_tgt - bludger.p1();
      Point tgt = dist(tgts.first, tp) < dist(tgts.second, tp) ? tgts.first : tgts.second;

      bludger_tgt = Point{ bludger.p.x + tgt.x, bludger.p.y + tgt.y };
      bludger_thrust = max_accel;
    }
  }

  // Activate shield if we're going fast enough to do damage and a collision is iminent.
  if (bludger.speed() > 2*max_accel &&
      (bludger.will_collide_with(enemies[0]) ||
       bludger.will_collide_with(enemies[1])))
    bludger_thrust = -1;

  cout << bludger_tgt.x << " " << bludger_tgt.y << " ";
  if (bludger_thrust == -1)
    cout << "SHIELD" << endl;
  else
    cout << bludger_thrust << endl;
}

int main()
{
  int nr_laps;
  cin >> nr_laps; cin.ignore();
  int nr_cps; // # of checkpoints
  cin >> nr_cps; cin.ignore();
  vector<Point> cps(nr_cps); // checkpoints
  for (int c = 0; c < nr_cps; c++) {
    cin >> cps[c].x >> cps[c].y; cin.ignore();
  }

  array<Pod, 2> pods;
  array<Pod, 2> enemies;

  while (true) {
    // Read data
    pods[0].read_from_stdin();
    pods[1].read_from_stdin();

    enemies[0].read_from_stdin();
    enemies[1].read_from_stdin();

    auto tm1 = chrono::high_resolution_clock::now();

    // Let agents act
    cerr << "pod1 ";
    speeder_AI(pods[0], enemies, cps);

    cerr << "pod2 ";
    //speeder_AI(pods[1], enemies, cps);
    bludger_AI(pods[1], pods[0], enemies, cps);

    auto tm2 = chrono::high_resolution_clock::now();
    auto calc_time = chrono::duration_cast<chrono::microseconds>(tm2 - tm1);
    cerr << "Calc time: "<< calc_time.count() << "us\n";
  }
}
