#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <tuple>

using namespace std;

struct Point
{
  int r, c; // row, column
};

bool operator== (Point a, Point b) { return a.r == b.r && a.c == b.c; }
bool operator!= (Point a, Point b) { return a.r != b.r || a.c != b.c; }
bool operator< (Point a, Point b) { return tie(a.r, a.c) < tie(b.r, b.c); }
Point operator+ (Point a, Point b) { return Point{a.r + b.r, a.c + b.c}; }
Point operator- (Point a, Point b) { return Point{a.r - b.r, a.c - b.c}; }

Point invalid_point() { return Point{numeric_limits<int>::max(),numeric_limits<int>::max()}; }
Point right() { return Point{ 0, 1}; }
Point left()  { return Point{ 0,-1}; }
Point up()    { return Point{-1, 0}; }
Point down()  { return Point{ 1, 0}; }

// Note: non Euclidean
int dist(Point a, Point b) { return abs(b.r - a.r) + abs(b.c - a.c); }

using Path = vector<Point>;

using Maze = vector<string>;

bool can_move(Point a, Maze const & maze)
{
  return maze[a.r][a.c] != '#';
}

// A* search
Path shortest_path(Point a, Point b, Maze const & maze)
{
  vector<Path> frontier{Path{a}};
  set<Point> visited;
  while (!frontier.empty()) {
    // 1) Find best candidate
    int best_score = numeric_limits<int>::max();
    int best_f;
    Point best_p;
    for (unsigned f = 0; f < frontier.size(); ++f) {
      int score = frontier[f].size() + dist(frontier[f].back(), b);
      if (score < best_score) {
        best_score = score;
        best_f = f;
      }
    }

    // 2) Found target, or an unknown?
    Point p = frontier[best_f].back();
    if (p == b)
      return frontier[best_f];
    if (maze[p.r][p.c] == '?')
      return frontier[best_f];

    // 3) Expand this path
    auto v = visited.insert(p);
    vector<Path> new_paths;
    if (v.second) {
      if (can_move(p + right(), maze) && visited.count(p + right()) == 0) {
        new_paths.push_back(frontier[best_f]);
        new_paths.back().push_back(p + right());
      }
      if (can_move(p + left(), maze) && visited.count(p + left()) == 0) {
        new_paths.push_back(frontier[best_f]);
        new_paths.back().push_back(p + left());
      }
      if (can_move(p + up(), maze) && visited.count(p + up()) == 0) {
        new_paths.push_back(frontier[best_f]);
        new_paths.back().push_back(p + up());
      }
      if (can_move(p + down(), maze) && visited.count(p + down()) == 0) {
        new_paths.push_back(frontier[best_f]);
        new_paths.back().push_back(p + down());
      }
    }
    frontier.erase(frontier.begin() + best_f);
    copy(new_paths.begin(), new_paths.end(), back_inserter(frontier));
  }

  return Path{};
}

void draw_path(Path const & path, Maze & maze)
{
  for (Point p : path)
    if (maze[p.r][p.c] == '.')
      maze[p.r][p.c] = 'X';
}

// Determines a target location to explore.
Point explore_target(Maze const & maze)
{
  constexpr int scan_diam = 5;
  constexpr int scan_radius = scan_diam / 2;
  for (int r = scan_radius; r < maze.size() - scan_radius; r += scan_diam)
    for (int c = scan_radius; c < maze[0].size() - scan_radius; c += scan_diam)
      for (int r1 = -scan_radius; r1 < scan_radius; ++r1)
        for (int c1 = -scan_radius; c1 < scan_radius; ++c1)
          if (maze[r + r1][c + c1] == '?')
            return Point{r, c};
  return invalid_point();
}

int main()
{
  int nr_rows;
  int nr_cols;
  int alarm_turns;
  cin >> nr_rows >> nr_cols >> alarm_turns; cin.ignore();

  Maze maze(nr_rows);
  for (string & row : maze)
    row.resize(nr_cols);
  Point tlprt = invalid_point(); // location of teleporter
  Point ctrl = invalid_point(); // location of control room
  bool back_to_T = false;

  while (true) {
    // 1) Read maze and teleporter & control room locations.
    Point k; // kirk
    cin >> k.r >> k.c; cin.ignore();
    for (int r = 0; r < nr_rows; r++) {
      cin >> maze[r]; cin.ignore();
      for (int c = 0; c < nr_cols; ++c) {
        if (maze[r][c] == 'T')
          tlprt = Point{r,c};
        else if (maze[r][c] == 'C')
          ctrl = Point{r,c};
      }
    }
    if (k == ctrl) {
      // Kirk found the control room, move back to the teleporter.
      back_to_T = true;
    }

    // 2) Add additional knowledge (edges are walls)
    for (int c = 0; c < nr_cols; ++c) {
      maze[0][c] = '#';
      maze[nr_rows - 1][c] = '#';
    }
    for (int r = 1; r < nr_rows - 1; ++r) {
      maze[r][0] = '#';
      maze[r][nr_cols - 1] = '#';
    }

    // 3) Find path to move Kirk
    Path path;
    if (ctrl == invalid_point()) {
      // Search for control room
      path = shortest_path(k, explore_target(maze), maze);
    }
    else if (back_to_T) {
      // Navigate back to T.
      path = shortest_path(k, tlprt, maze);
    }
    else {
      // Before going into the control room, make sure the shortest way back is found.
      auto return_path = shortest_path(ctrl, tlprt, maze);
      Point rp = return_path.back();
      if (maze[rp.r][rp.c] != 'T') {
        // Find better path to transporter first
        path = shortest_path(k, rp, maze);
      }
      else {
        // Navigate to control room
        path = shortest_path(k, ctrl, maze);
      }
    }

    // 4) Write debug output and command for Kirk.
    draw_path(path, maze);
    for (string const & row : maze)
      cerr  << row << endl;

    Point dir = path[1] - path[0];
    if (dir == right())
      cout << "RIGHT" << endl;
    else if (dir == left())
      cout << "LEFT" << endl;
    else if (dir == up())
      cout << "UP" << endl;
    else if (dir == down())
      cout << "DOWN" << endl;
  }
}
